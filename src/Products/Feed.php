<?php

namespace Tradedoubler\Products;

/*
Name        Description                                             Type        Required
========================================================================================
pretty      Set to true to get nice line breaks in the response.    Boolean     No
*/

use Tradedoubler\Api\AClient;

/**
* Feed action on Tradedoublers products api
*
* @package Tradedoubler
* @author Andreas Sundqvist <andreas@forme.se>
* @copyright Copyright © 2014 Andreas Sundqvist
*/
class Feed extends AClient {
    protected $url = 'http://api.tradedoubler.com/1.0/productFeeds{format}{query}?token={token}';

    public function get() {
        return $this->makeRequest('GET');
    }
}
