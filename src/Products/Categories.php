<?php
namespace Tradedoubler\Products;

/*
Name        Description                                                 Required
================================================================================
language    ISO 639-1 code of the language to use in the response. 
            For example en for English or sv for Swedish.               No
*/

use Tradedoubler\Api\AClient;

/**
* Categories action on Tradedoublers products api
*
* @package Tradedoubler
* @author Andreas Sundqvist <andreas@forme.se>
* @copyright Copyright © 2014 Andreas Sundqvist
*/
class Categories extends AClient {
    protected $url = 'http://api.tradedoubler.com/1.0/productCategories{format}{query}?token={token}';

    public function get($lang = 'sv') {
        return $this->makeRequest('GET', [
            'language'=>$lang
        ]);
    }
}
