<?php
/*
Name                Description                                                                 Type            Required
========================================================================================================================
fid                 Primary key of the feed you are requesting.                                 Integer         Yes
minUpdateDate       Only return products which has been modified after this point in time.      Integer/Date    No
maxUpdateDate       Only return products which has been modified before this point in time.     Integer/Date    No
includeDeleted      Set to true to include deleted products in the reponse.                     Boolean         No
pretty              Set to true for nice line breaks in the output.                             Boolean         No
*/

namespace Tradedoubler\Products;

use Tradedoubler\Api\AClient;
use DateTimeInterface;
use DateTime;

/**
* Unlimited action on Tradedoublers products api
*
* @package Tradedoubler
* @author Andreas Sundqvist <andreas@forme.se>
* @copyright Copyright © 2014 Andreas Sundqvist
*/
class Unlimited extends AClient {
    protected $url = 'http://api.tradedoubler.com/1.0/productsUnlimited{format}{query}?token={token}';

    public function get($feedId, $startDate=false, $endDate=false) {
        $query = [
            'fid'=>$feedId
        ];
        if ($startDate) {
            if (!($startDate instanceOf DateTimeInterface)) {
                $startDate = new DateTime($startDate);
            }
            $query['minUpdateDate'] = $startDate->format('Y-m-d');
        }
        if ($endDate) {
            if (!($endDate instanceOf DateTimeInterface)) {
                $endDate = new DateTime($endDate);
            }
            $query['maxUpdateDate'] = $endDate->format('Y-m-d');
        }
        return $this->makeRequest('GET', $query);
    }
}
