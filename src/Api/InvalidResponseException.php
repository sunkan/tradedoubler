<?php 

namespace Tradedoubler;

/**
* Invalid Response Exception
*
* @package Tradedoubler
* @author Andreas Sundqvist <andreas@forme.se>
* @copyright Copyright © 2014 Andreas Sundqvist
*/
class InvalidResponseException extends \Exception {}