<?php
namespace Tradedoubler\Api;

use Guzzle\Http\Client as HttpClient;
use Guzzle\Http\Message\RequestInterface;
use Guzzle\Http\Exception\ParseException;
use Guzzle\Http\Exception\ClientErrorResponseException;

/**
* Base class to access Tradedoublers api
*
* @package Tradedoubler
* @author Andreas Sundqvist <andreas@forme.se>
* @copyright Copyright © 2014 Andreas Sundqvist
*/
abstract class AClient {
    protected $token = '';
    protected $http_client = false;
    protected $url = false;
    protected $format = 'json';

    /**
     * 
     * @param string $token Api token
     * @param Guzzle\Http\Client $http_client
     */
    public function __construct($token, HttpClient $http_client) {
        $this->http_client = $http_client;
        $this->token = $token;
    }

    /**
     * Helper method to assamble url
     *
     * @param array $query query parameter to be inserted
     * @return string
     */
    protected function _createUrl(array $query=[]) {
        $search = [
            '{token}'=>$this->token, 
            '{format}'=>$this->format?'.'.$this->format:''
        ];
        if (stripos($this->url, '{query}')!==false) {
            $qs = '';
            foreach ($query as $key => $value) {
                $qs .= $key.'='.(is_array($value)?implode(',', $value):$value).';';
            }
            $search['{query}'] = ';'.trim($qs, ';');
        }
        return str_ireplace(array_keys($search), array_values($search), $this->url);
    }

    /**
     * MakeRequest to Api
     *
     * @param string $method Allowed values GET|POST|PUT|DELETE
     * @param array $query query parameter to api
     * @throws InvalidResponseException if response is not valid json
     * @return object Representing json response
     */
    public function makeRequest($method, array $query=[]) {
        $request = $this->http_client->createRequest($method, $this->_createUrl($query));
        try {
            $this->response = $this->http_client->send($request);
        } catch(ClientErrorResponseException $cere) {
            $this->response = $cere->getResponse();
        }

        try {
            return $this->response->json();
        } catch(\Exception $re) {
            throw new InvalidResponseException('Invalid json response');
        }
    }
}