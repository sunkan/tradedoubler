<?php

namespace Tradedoubler;

/**
* Report api for tradedoubler
*
* @package Tradedoubler
* @author Andreas Sundqvist <andreas@forme.se>
* @copyright Copyright © 2014 Andreas Sundqvist
*/
class ReportApi {
    private $format = 'csv';
    private $api_token = '';
    private $affiliate_id = 0;

    /**
     *
     * @param string $token Api token
     * @param string $affiliate_id Your affiliate id 
     */
    public function __construct($token, $affiliate_id) {
        $this->api_token = $token;
        $this->affiliate_id = $affiliate_id;
    }

    private function createQuery($query) {
        if ($this->affiliate_id) {
            $query['affiliateId'] = $this->affiliate_id;
        }
        $query['format'] = $this->format;
        $query['key'] = $this->api_token;

        $string = '';
        foreach ($query as $key => $value) {
            $string .= $key.'=';
            if (is_array($value)) {
                $string .= implode('&'.$key.'=', $value);
            } else {
                $string .= urlencode($value);
            }
            $string .= '&';
        }
        return trim($string, '&');
    }

    /**
     * Set response format
     *
     * @param string $format Valid formats CSV|HTML|PDF|EXCEL|XML
     * @return ReportApi
     */
    public function setFormat($format) {
        $this->format = strtoupper($this->format);
        return $this;
    }

    /**
     * Get overview report
     *
     * @return 
     */
    public function getOverview() {
        $query['reportName']='mMerchantOverviewReport';
        
        $query['startDate']='2014-10-01';
        $query['endDate']='2014-10-17';
        
        $query['programId']=98462;
        $query['run_as_organization_id']=198758;
        
        $query['columns']=[
            'siteName',
            'siteId',
            'impNrOf',
            'clickNrOf',
            'clickRate',
            'uvNrOf',
            'leadNrOf',
            'leadRate',
            'saleNrOf',
            'saleCommission',
            'conversionRate',
            'cpl',
            'cpo',
            'aov',
            'affiliateCommission',
            'link'
        ];
        $query['applyNamedDecorator']='true';
        $query['decorator']='popupDecorator';
        $query['allPrograms']='false';
        $query['viewType']=1;
        $query['currencyId']='SEK';

        return $this->createQuery($query);
    }

    public function getEpiReport($domain) {
        $query['reportName'] = 'aAffiliateEPIReport';

        $query['epi1']=$domain;

        $query['startDate']='2014-10-01';
        $query['endDate']='2014-10-17';

        $query['columns']=[
            'epi1',
            'totalOrderValue',
            'affiliateCommission',
            'link',
            'timeStamp'
        ];
        $query['currencyId']='SEK';
        $query['decorator']='popupDecorator';

        $query['setColumns']='true';
        $query['applyNamedDecorator']='true';
        $query['includeWarningColumn']='true';

        $query['view']=2;
        $query['latestDayToExecute']=0;
        $query['customKeyMetricCount']=0;

        $query['reportTitleTextKey']='REPORT3_SERVICE_REPORTS_AAFFILIATEEPIREPORT_TITLE';

        $query['metric1.columnName1']='programId';
        $query['metric1.columnName2']='programId';
        $query['metric1.summaryType']='NONE';
        $query['metric1.lastOperator']='/';
        $query['metric1.midOperator']='/';
        $query['metric1.operator1']='/';

        return $this->createQuery($query);
    }

    public function getProgramOverview() {
        $query['reportName']='aAffiliateProgramOverviewReport';
        
        $query['startDate']='2014-10-01';
        $query['endDate']='2014-10-17';
        
        $query['columns']=[
            'programId',
            'impNrOf',
            'uimpNrOf',
            'clickNrOf',
            'clickRate',
            'uvNrOf',
            'uvRate',
            'leadNrOf',
            'leadRate',
            'saleNrOf',
            'saleCommission',
            'conversionRate',
            'totalOrderValue',
            'affiliateCommission',
            'link'
        ];
        $query['currencyId']='SEK';
        $query['interval']='MONTHS';
        $query['decorator']='popupDecorator';

        $query['latestDayToExecute']=0;
        $query['customKeyMetricCount']=0;

        $query['setColumns']='true';
        $query['applyNamedDecorator']='true';
        $query['includeWarningColumn']='true';

        $query['reportTitleTextKey']='REPORT3_SERVICE_REPORTS_AAFFILIATEPROGRAMOVERVIEWREPORT_TITLE';

        $query['metric1.columnName1']='programId';
        $query['metric1.columnName2']='programId';
        $query['metric1.summaryType']='NONE';
        $query['metric1.lastOperator']='/';
        $query['metric1.midOperator']='/';
        $query['metric1.operator1']='/';

        return $this->createQuery($query);
    }

    public function getEventBreakdown(){
        $query['reportName']='aAffiliateEventBreakdownReport';
        
        $query['startDate']='2014-10-01';
        $query['endDate']='2014-10-17';

        $query['organizationId']=1861294;
        
        $query['columns']=[
            'timeOfVisit',
            'timeOfEvent',
            'timeInSession',
            'lastModified',
            'epi1',
            'eventName',
            'pendingStatus',
            'siteName',
            'graphicalElementName',
            'productName',
            'productNrOf',
            'productValuem',
            'open_product_feeds_id',
            'open_product_feeds_name',
            'voucher_code',
            'deviceType',
            'os',
            'browser',
            'vendor',
            'device',
            'affiliateCommission',
            'link',
            'leadNR',
            'orderNR',
            'pendingReason',
            'orderValue'
        ];
        $query['currencyId']='SEK';
        $query['decorator']='popupDecorator';

        $query['setColumns']='true';
        $query['applyNamedDecorator']='true';
        $query['includeWarningColumn']='true';
        $query['filterOnTimeHrsInterval']='false';
        
        $query['event_id']=0;
        $query['includeMobile']=1;
        $query['pending_status']=1;
        $query['breakdownOption']=1;
        $query['dateSelectionType']=1;
        $query['latestDayToExecute']=0;
        $query['customKeyMetricCount']=0;
        
        $query['reportTitleTextKey']='REPORT3_SERVICE_REPORTS_AAFFILIATEEVENTBREAKDOWNREPORT_TITLE';
        
        $query['metric1.columnName1']='orderValue';
        $query['metric1.columnName2']='orderValue';
        $query['metric1.summaryType']='NONE';
        $query['metric1.lastOperator']='/';
        $query['metric1.midOperator']='/';
        $query['metric1.operator1']='/';
        
        $query['sortBy']='timeOfEvent';

        return $this->createQuery($query);
  }
}
